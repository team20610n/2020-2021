#include "main.h"

void print(std::string text) {
    Logger::getDefaultLogger()->info([&]{return text;});
}

void initialize() {
  Logger::setDefaultLogger(
    std::make_shared<Logger>(
        TimeUtilFactory::createDefault().getTimer(), // It needs a Timer
        "/ser/sout", // Output to the PROS terminal
        // "/usd/test_logging.txt", // Output to the sdcard.
        Logger::LogLevel::debug // Show errors and warnings
    )
  );
  print("Initialization completed");
	runScreen();
}

void disabled() {
  print("Robot disabled");
	lv_tabview_set_tab_act(tabview, 1, true);
  ChassisControl_TR.suspend();
	chassis.get()->stop();
}

void competition_initialize() {print("Initialized in competition mode");}
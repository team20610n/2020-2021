#include "main.h"

// controller declaration
Controller master (ControllerId::master);
Controller partner (ControllerId::partner);

DistanceSensor TopBallDetector(19);
DistanceSensor BottomBallDetector(5);

Motor RightRollerMotor(6, true, AbstractMotor::gearset::green, AbstractMotor::encoderUnits::degrees);
Motor LeftRollerMotor(21, false, AbstractMotor::gearset::green, AbstractMotor::encoderUnits::degrees);
Motor Intake(13, true, AbstractMotor::gearset::blue, AbstractMotor::encoderUnits::degrees);
Motor OutTake(11, true, AbstractMotor::gearset::blue, AbstractMotor::encoderUnits::degrees);
MotorGroup Roller({21, -6});

int startingAnglerAngle;


void Diagnostics(void* param) {
	while (true) {
    //Updates display values.
    // updateLineVariable(1, Gyro);
		updateLineVariable(2, TopBallDetector.get());
    updateLineVariable(3, BottomBallDetector.get());
		updateLineVariable(4, leftEncoder.get());
		updateLineVariable(5, rightEncoder.get());
    
    // Graph drive motor temps
    lv_chart_set_next(chart, NavyLine, frontLeftMotor.getTemperature());
    lv_chart_set_next(chart, BlueLine, backLeftMotor.getTemperature());
    lv_chart_set_next(chart, GreenLine, frontRightMotor.getTemperature());
    lv_chart_set_next(chart, LimeLine, backRightMotor.getTemperature());
    // Graph other motors
    lv_chart_set_next(chart, SilverLine, LeftRollerMotor.getTemperature());
    lv_chart_set_next(chart, WhiteLine, RightRollerMotor.getTemperature());
    lv_chart_set_next(chart, PurpleLine, Intake.getTemperature());
    lv_chart_set_next(chart, OrangeLine, OutTake.getTemperature());
    pros::delay(100);
	}
}

void opcontrol() {
  print("Opcontrol started");
  lv_tabview_set_tab_act(tabview, 3, true);
  chassis->setTurnsMirrored(false);
  // Seting the motor brake mode to hold
  Intake.setBrakeMode(AbstractMotor::brakeMode::hold);
  OutTake.setBrakeMode(AbstractMotor::brakeMode::hold);

  // Start the thread for chassis
  if (ChassisControl_TR.get_state() != 2) {
    ChassisControl_TR.resume();
    print("ChassisControl resumed");
  }

  while (true) {
    if (master.getDigital(ControllerDigital::L1)) {
      Roller.moveVelocity(200);
      
      if (BottomBallDetector.get() > 60 || BottomBallDetector.get() == -1) {
        Intake.moveVelocity(600);
        OutTake.moveVelocity(600);
      }
      else {
        Intake.moveVelocity(0);
        OutTake.moveVelocity(0);
      }
    }
    else if (master.getDigital(ControllerDigital::up)) {
      Intake.moveVelocity(200);
      OutTake.moveVelocity(100);
    }
    else if (master.getDigital(ControllerDigital::A)) {
      Roller.moveVelocity(200);
    }
    else if (master.getDigital(ControllerDigital::L2)) {
      Intake.moveVelocity(-600);
      Roller.moveVelocity(-200);
      OutTake.moveVelocity(-600);
    }

    if (master.getDigital(ControllerDigital::R2)) {
      Intake.moveVelocity(600);
      if (TopBallDetector.get() > 70) {
        OutTake.moveVelocity(-600);
      }
    }
    else if (master.getDigital(ControllerDigital::R1)) {
      OutTake.moveVelocity(600);
      Intake.moveVelocity(600);
    }

    if (!master.getDigital(ControllerDigital::L1) && !master.getDigital(ControllerDigital::L2) && !master.getDigital(ControllerDigital::R1) && !master.getDigital(ControllerDigital::R2) && !master.getDigital(ControllerDigital::up) && !master.getDigital(ControllerDigital::A)) {
      OutTake.moveVelocity(0);
      Intake.moveVelocity(0);
      Roller.moveVelocity(0);
    }

    pros::delay(20);
  }
  print("Opcontrol stopped");
}

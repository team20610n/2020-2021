#include "main.h"

RotationSensor leftEncoder(18, true);
RotationSensor rightEncoder(8, true);

bool chassisBrake = false;

Motor frontLeftMotor(9, false, AbstractMotor::gearset::green, AbstractMotor::encoderUnits::degrees);
Motor backLeftMotor(20, false, AbstractMotor::gearset::green, AbstractMotor::encoderUnits::degrees);
Motor frontRightMotor(10, true, AbstractMotor::gearset::green, AbstractMotor::encoderUnits::degrees);
Motor backRightMotor(17, true, AbstractMotor::gearset::green, AbstractMotor::encoderUnits::degrees);

std::shared_ptr<OdomChassisController> chassis =ChassisControllerBuilder()
    .withMaxVelocity(200)
    .withMaxRates(0.01, 0.023)
    .withMotors({9, 20}, {-10, -17})
    .withGains(
        {0.0012, 0, 0}, // distance controller gains
        {0.00248, 0, 0}, // turn controller gains
        {0.001, 0, 0.000004}  // angle controller gains (helps drive straight)
    )
    // green gearset, 4 inch wheel diameter, 11.5 inch wheelbase
    .withDimensions(AbstractMotor::gearset::green, {{4_in, 12_in}, imev5GreenTPR})
    .withSensors(leftEncoder, rightEncoder)
    // specify the tracking wheels diameter (2.75 in), track (7 in), and TPR (360)
    .withOdometry({{4_in, 12_in}, quadEncoderTPR}, StateMode::FRAME_TRANSFORMATION)
    .buildOdometry();

// Thread that controls the chassis
void ChassisOpcontrol(void* param) {
  print("Chassis Controller thread inited");
  chassis.get()->stop();
  double CubedTurn = 0;
  double MasterY = 0;

  bool isBreaking = false;

  while (true) {
    std::cout << isBreaking << "\n";
    if (master.getDigital(ControllerDigital::down) && isBreaking == false) {
        backLeftMotor.moveRelative(0, 200);
        backRightMotor.moveRelative(0, 200);
        frontLeftMotor.moveRelative(0, 200);
        frontRightMotor.moveRelative(0, 200);
        isBreaking = true;
        pros::delay(200);
    }
    if (master.getDigital(ControllerDigital::down) && isBreaking == true) {
        isBreaking = false;
        chassis.get()->stop();
        pros::delay(200);
    }

    if (abs(master.getAnalog(ControllerAnalog::rightX)) > 0) {
      CubedTurn = ((master.getAnalog(ControllerAnalog::rightX) * master.getAnalog(ControllerAnalog::rightX) * master.getAnalog(ControllerAnalog::rightX) / 1.5));}
    else {CubedTurn = 0;}

    if (abs(master.getAnalog(ControllerAnalog::leftY)) > 0) {MasterY = master.getAnalog(ControllerAnalog::leftY);}
    else {MasterY = 0;}

    if (isBreaking == false) {chassis->getModel()->arcade(MasterY, CubedTurn, 0.1);}
    pros::delay(20);
  }
  print("Chassis Controller thread stopped.");
}

pros::Task ChassisControl_TR(ChassisOpcontrol, (void*)"PROS", TASK_PRIORITY_DEFAULT, TASK_STACK_DEPTH_DEFAULT, "ChassisControl");
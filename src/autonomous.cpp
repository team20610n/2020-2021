#include "main.h"

void CycleBalls(int Number, int Speed = 500) {
  print("Cycling: " + std::to_string(Number) + " ball at speed: " + std::to_string(Speed));
  Intake.moveVelocity(600); OutTake.moveVelocity(Speed);
  int a = 1;
  for (int i = 0; i < Number; ++i) {
    // Continues if ball
    while (TopBallDetector.get() > 50) {
      if (a > 100) {break;}
      pros::delay(20);
      a = a+1;
    }
    if (a > 100) {print("Ball not found timing out");} else {print("Found ball");}
    a = 1;
    // if (i == 1){Intake.moveVelocity(0);}
    // Continues if no ball
    while (TopBallDetector.get() < 50) {
      if (a > 100) {break;}
      pros::delay(20);
      a = a+1;
    }
    if (a > 100) {print("Couldn't release ball timing out");} else {print("Released ball");}
    a = 1;
  }
  Intake.moveVelocity(0); 
  OutTake.moveVelocity(0);
}

bool stopBallAtTop = false;
bool stopBallAtBottom = false;
int irollerSpeed = 200;
int iintakeSpeed = 400;
void pickUpBall(std::string selector = "bottom", int rollerSpeed = 200, int intakeSpeed = 400) {
  print("Function pickUpBall: " + selector + " Position rollerSpeed: " + std::to_string(rollerSpeed) + "intakeSpeed: " + std::to_string(intakeSpeed));
  if (selector == "top") {
      stopBallAtTop = true;
      irollerSpeed = rollerSpeed;
      iintakeSpeed = intakeSpeed;
  }
  else if (selector == "bottom") {
    stopBallAtBottom = true;
      irollerSpeed = rollerSpeed;
      iintakeSpeed = intakeSpeed;
  }
}

void intakeController(void* param) {
  while (true) {
    if (stopBallAtTop == true) {
      print("Watching for top ball");
      Roller.moveVelocity(irollerSpeed); Intake.moveVelocity(iintakeSpeed); OutTake.moveVelocity(330);
      while (TopBallDetector.get() > 30) {
        if (stopBallAtTop == false) {print("Canceled cause: variable set to false"); break;}
        pros::delay(20);
      }
      if (stopBallAtTop == true) {print("Ball found");}
      Roller.moveVelocity(0); Intake.moveVelocity(0); OutTake.moveVelocity(0);
      stopBallAtTop = false;
    }
    if (stopBallAtBottom == true) {
      print("Watching for bottom ball");
      Roller.moveVelocity(irollerSpeed); Intake.moveVelocity(iintakeSpeed);
      while (BottomBallDetector.get() > 80) {
        if (stopBallAtBottom == false) {print("Canceled cause: variable set to false"); break;}
        pros::delay(20);
      }
      if (stopBallAtBottom == true) {print("Ball found");}
      Roller.moveVelocity(0); Intake.moveVelocity(0); OutTake.moveVelocity(0);
      stopBallAtBottom = false;
    }
    pros::delay(30);
  }
}
pros::Task intakeController_TR(intakeController, (void*)"PROS", TASK_PRIORITY_DEFAULT, TASK_STACK_DEPTH_DEFAULT, "Intake Controller");

void moveTilStall(okapi::QLength target, int speed = 200, int startingThreshhold = 50) {
  print("Moving until stall");
  int pastSpeed = chassis.get()->getMaxVelocity();
  chassis.get()->setMaxVelocity(speed);
  chassis->moveDistanceAsync(target);
  while (abs(backLeftMotor.getActualVelocity()) < startingThreshhold) {
    pros::delay(20);
    // if (backLeftMotor.getActualVelocity() > 5) {print("true");} else {print("false");}
  }
  print("Moving... watching for stall");
  while (abs(backLeftMotor.getActualVelocity()) > 20 && abs(backRightMotor.getActualVelocity()) > 20 && abs(frontRightMotor.getActualVelocity()) > 20 && abs(frontLeftMotor.getActualVelocity()) > 20) {
    pros::delay(20);
    // print(std::to_string(backLeftMotor.getActualVelocity()));
  }
  chassis->stop();
  chassis.get()->setMaxVelocity(pastSpeed);
  print("Finished");
}

void poopBall() {
  int count = 1;
  while (count > 0) {
    Intake.moveVelocity(600);
    if (TopBallDetector.get() > 70) {
      OutTake.moveVelocity(-600);
    }
    while (BottomBallDetector.get() < 70) {pros::delay(20);}
    count -= 1;
  }
  OutTake.moveVelocity(0); Intake.moveVelocity(0);
}



void blueSafe() {
  CycleBalls(2);
}

// field 141_in
void Skills () {
  bool gotFirstBlue = false;

  // goal D
  chassis->setState({70.5_in, 129_in, 90_deg});
  CycleBalls(1);
  // moveTilStall(4_in, 100, 17);
  chassis->moveDistanceAsync(4_in);
  pros::delay(500);
  chassis->stop();
  pickUpBall();
  chassis.get()->setMaxVelocity(150);
  chassis->driveToPoint({70.5_in, 123_in}, true);
  if (BottomBallDetector.get() < 70) {gotFirstBlue = true;}
  pickUpBall("top");


  // goal G
  pickUpBall();
  chassis->driveToPoint({112_in, 116_in});
  chassis->turnAngle(-50_deg);
  if (gotFirstBlue == true) {CycleBalls(1);}
  
  
  chassis.get()->setMaxVelocity(120);
  chassis->turnToPoint({128.5_in, 141_in});
  stopBallAtBottom = false; stopBallAtTop = false;
  moveTilStall(3_ft, 70);
  CycleBalls(1);
  // pickUpBall();
  // pros::delay(800);
  // chassis->moveDistance(-12_in);
  chassis->driveToPoint({114_in, 122_in}, true);

  // to goal A
  chassis->turnToPoint({25_in, 116.75_in});
  poopBall();
  stopBallAtBottom = false;
  pros::delay(40);

  pickUpBall();
  chassis.get()->setMaxVelocity(130);
  chassis->driveToPoint({25_in, 116.25_in});
  chassis->turnToPoint({15_in, 146_in});
  moveTilStall(2_ft, 100);
  CycleBalls(1);
  // pickUpBall();
  // pros::delay(1000);
  
  chassis.get()->setMaxVelocity(150);
  chassis->driveToPoint({44_in, 99_in}, true);
  Roller.moveVelocity(-200);
  stopBallAtBottom = false;
  pros::delay(300);
  Roller.moveVelocity(0);

  // goal B
  chassis.get()->setMaxVelocity(120);
  chassis->turnToPoint({46_in, 80.5_in});
  poopBall();
  pickUpBall("top");
  chassis->driveToPoint({46.5_in, 80.5_in});
  chassis->turnToPoint({1_in, 93.5_in});
  pickUpBall();
  chassis->setMaxVelocity(70);
  moveTilStall(4_ft, 70);
  chassis->setMaxVelocity(120);
  stopBallAtBottom = false; stopBallAtTop = false;
  pros::delay(500);
  CycleBalls(2);

  pickUpBall();
  pros::delay(1400);
  Roller.moveVelocity(-60);
  chassis->driveToPoint({35_in, 85_in}, true);
  Roller.moveVelocity(0);
  chassis->turnToPoint({35.25_in, 23.5_in});
  poopBall();
  stopBallAtBottom = false;
  pros::delay(40);

  // goal C
  pickUpBall();
  chassis.get()->setMaxVelocity(150);
  chassis->driveToPoint({30_in, 33_in});
  chassis->driveToPoint({30_in, 42_in}, true);

  chassis->turnToPoint({1_in, 28_in});
  moveTilStall(4.5_ft, 120);
  stopBallAtTop = false;
  CycleBalls(1);
  pickUpBall();
  pros::delay(1000);
  chassis->moveDistance(-1_ft);
}

void redSafe() {
  chassis->setState({70.5_in, 129_in, 90_deg});
  CycleBalls(1);
  chassis->driveToPoint({70.5_in, 98_in}, true);
  
  // chassis->driveToPoint({111_in, 128_in});
  Roller.moveVelocity(100);
  chassis->driveToPoint({121_in, 136_in});
  // moveTilStall(2.3_ft);
  Roller.moveVelocity(-200);
  
  chassis->moveDistance(-1_ft);
  Roller.moveVelocity(0);
  chassis->driveToPoint({38_in, 120_in});
  chassis->turnToPoint({5_in, 141_in});

  Roller.moveVelocity(100);
  moveTilStall(4.3_ft);
  Roller.moveVelocity(0);
  pros::delay(2000);
}

void blueBlue() {
  chassis->setState({70.5_in, 12_in, 270_deg});
  CycleBalls(1);
  chassis->driveToPoint({70.5_in, 40_in}, true);
  chassis->driveToPoint({114_in, 15_in});

  Roller.moveVelocity(200);
  moveTilStall(2.3_ft);
  Roller.moveVelocity(0);
  pros::delay(2000);
}

void blueRed() {
  chassis->setState({70.5_in, 12_in, 270_deg});
  CycleBalls(1);
  chassis->driveToPoint({70.5_in, 40_in}, true);
  chassis->driveToPoint({34_in, 15.5_in});

  Roller.moveVelocity(200);
  moveTilStall(2.3_ft);
  Roller.moveVelocity(0);
  pros::delay(2000);
}

void redBlue() {
  chassis->setState({70.5_in, 129_in, 90_deg});
  CycleBalls(1);
  chassis->driveToPoint({70.5_in, 98_in}, true);
  chassis->driveToPoint({111_in, 126_in});

  Roller.moveVelocity(200);
  moveTilStall(2.3_ft);
  Roller.moveVelocity(0);
  pros::delay(2000);
}

void redRed() {
  chassis->setState({70.5_in, 129_in, 90_deg});
  CycleBalls(1);
  chassis->driveToPoint({70.5_in, 98_in}, true);
  chassis->driveToPoint({30_in, 126_in});

  Roller.moveVelocity(200);
  moveTilStall(2.3_ft);
  Roller.moveVelocity(0);
  pros::delay(2000);
}

std::string selectedAuton = "Skills";
// Part of the auton selector code
void autonomous() {
  if (ChassisControl_TR.get_state() != 3) {
    ChassisControl_TR.suspend();
    print("ChassisControl suspended");
  }
  print("State of Chassis controller thread: " + std::to_string(ChassisControl_TR.get_state()));
  print("Starting Auton: " + selectedAuton);

  lv_tabview_set_tab_act(tabview, 2, true);
  if (strcmp(selectedAuton.c_str(), "BSaf") == 0) {
    blueSafe();
  }
  else if (strcmp(selectedAuton.c_str(), "RSaf") == 0) {
    redSafe();
  }
  else if (strcmp(selectedAuton.c_str(), "Bb") == 0) {
    blueBlue();
  }
  else if (strcmp(selectedAuton.c_str(), "Br") == 0) {
    blueRed();
  }
  else if (strcmp(selectedAuton.c_str(), "Rb") == 0) {
    redBlue();
  }
  else if (strcmp(selectedAuton.c_str(), "Rr") == 0) {
    redRed();
  }
  else if (strcmp(selectedAuton.c_str(), "Skills") == 0) {
    Skills();
  }
}